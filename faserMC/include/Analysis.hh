#ifndef MyAnalysis_h
#define MyAnalysis_h 1
#include "g4root.hh"
#endif
#include "G4Types.hh"
#include <vector>

extern G4ThreadLocal double e_beam;
extern G4ThreadLocal int id_beam;
extern G4ThreadLocal double x_beam;
extern G4ThreadLocal double y_beam;
extern G4ThreadLocal int pdgnu_nuEvt;
extern G4ThreadLocal int pdglep_nuEvt;
extern G4ThreadLocal double Enu_nuEvt;
extern G4ThreadLocal double Plep_nuEvt;
extern G4ThreadLocal int cc_nuEvt;
extern G4ThreadLocal double x_nuEvt;
extern G4ThreadLocal double y_nuEvt;
extern G4ThreadLocal double z_nuEvt;
extern G4ThreadLocal std::vector<int> cham;
extern G4ThreadLocal std::vector<int> idz;
extern G4ThreadLocal std::vector<int> idzsub;
extern G4ThreadLocal std::vector<int> pdgid;
extern G4ThreadLocal std::vector<int> id;
extern G4ThreadLocal std::vector<int> idParent;
extern G4ThreadLocal std::vector<double> charge;
extern G4ThreadLocal std::vector<double> x;
extern G4ThreadLocal std::vector<double> y;
extern G4ThreadLocal std::vector<double> z;
extern G4ThreadLocal std::vector<double> px;
extern G4ThreadLocal std::vector<double> py;
extern G4ThreadLocal std::vector<double> pz;
extern G4ThreadLocal std::vector<double> e1;
extern G4ThreadLocal std::vector<double> e2;
extern G4ThreadLocal std::vector<double> len;
extern G4ThreadLocal std::vector<double> edep;
