// adapted from Geant4 example

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "G4UImanager.hh"
#include "FTFP_BERT.hh"
#include "FaserVisAction.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "Randomize.hh"

#include "FaserDetectorConstruction.hh"
#include "FaserActionInitialization.hh"
#include "FaserPhysicsList.hh"
#include "RootEventIO.hh"
#include "TSystem.h"
 #include "TROOT.h"
 #include "TFile.h"
 #include "TTree.h"
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void tree1r()
 {
    gSystem->Load("libFaserRootClassesDict.so");
    gSystem->Load("libFaserTrackerClassesDict.so");
//    TFile *f = new TFile("out.root");
//    TTree *faser = (TTree*)f->Get("faser");
//    TBranch *e = (TBranch*)faser->GetBranch("event");
//    TLeaf *number = (TLeaf*)e->GetLeaf("fEventNumber");
    TFile *f = new TFile("tree1.root");
    TTree *t1 = (TTree*)f->Get("t1");
    TBranch *b1 = (TBranch*)t1->GetBranch("pz");
    Float_t pz;
//    Double_t random;
//    Int_t fEventNumber;
//    FaserEvent event;
//    t1->SetBranchAddress("px",&px);
//    t1->SetBranchAddress("py",&py);
//    TBranch *b1 = (TBranch*)t1->GetBranch("pz");
//    number->SetAddress(&fEventNumber);
//      faser->SetBranchAddress("event",&event);

    b1->SetAddress(&pz);
//    t1->SetBranchAddress("random",&random);
//    t1->SetBranchAddress("ev",&ev);

//    Long64_t nentries = t1->GetEntries();
    Long64_t nentries = 1;
    for (Long64_t i=0;i<nentries;i++) {
      t1->GetEntry(i);

      std::cout << "Entry " << i << " has pz " << pz << std::endl;
   }
 }

int main()
{
  tree1r();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
