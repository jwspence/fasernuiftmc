// adapted from Geant4 example

#include "FaserRunAction.hh"
#include "FaserPrimaryGeneratorAction.hh"
#include "FaserDetectorConstruction.hh"
// #include "FaserGeometry.hh"
#include "FaserRun.hh"
#include "RootEventIO.hh"
//JS
#include "Analysis.hh"
//JS
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
G4ThreadLocal double e_beam;
G4ThreadLocal int id_beam;
G4ThreadLocal double x_beam;
G4ThreadLocal double y_beam;
G4ThreadLocal int pdgnu_nuEvt;
G4ThreadLocal int pdglep_nuEvt;
G4ThreadLocal double Enu_nuEvt;
G4ThreadLocal double Plep_nuEvt;
G4ThreadLocal int cc_nuEvt;
G4ThreadLocal double x_nuEvt;
G4ThreadLocal double y_nuEvt;
G4ThreadLocal double z_nuEvt;
G4ThreadLocal std::vector<int> cham;
G4ThreadLocal std::vector<int> idz;
G4ThreadLocal std::vector<int> idzsub;
G4ThreadLocal std::vector<int> pdgid;
G4ThreadLocal std::vector<int> id;
G4ThreadLocal std::vector<int> idParent;
G4ThreadLocal std::vector<double> charge;
G4ThreadLocal std::vector<double> x;
G4ThreadLocal std::vector<double> y;
G4ThreadLocal std::vector<double> z;
G4ThreadLocal std::vector<double> px;
G4ThreadLocal std::vector<double> py;
G4ThreadLocal std::vector<double> pz;
G4ThreadLocal std::vector<double> e1;
G4ThreadLocal std::vector<double> e2;
G4ThreadLocal std::vector<double> len;
G4ThreadLocal std::vector<double> edep;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FaserRunAction::FaserRunAction()
  : G4UserRunAction()
{ 
//JS
  G4RunManager::GetRunManager()->SetPrintProgress(1);     

 auto analysisManager = G4AnalysisManager::Instance();
  G4cout << "Using " << analysisManager->GetType() << G4endl;

  analysisManager->SetVerboseLevel(1);
  analysisManager->SetNtupleMerging(true);

  analysisManager->CreateNtuple("FASERnu", "");
  analysisManager->CreateNtupleDColumn("e_beam");
  analysisManager->CreateNtupleIColumn("id_beam");
  analysisManager->CreateNtupleDColumn("x_beam");
  analysisManager->CreateNtupleDColumn("y_beam");
  analysisManager->CreateNtupleIColumn("pdgnu_nuEvt");
  analysisManager->CreateNtupleIColumn("pdglep_nuEvt");
  analysisManager->CreateNtupleDColumn("Enu_nuEvt");
  analysisManager->CreateNtupleDColumn("Plep_nuEvt");
  analysisManager->CreateNtupleIColumn("cc_nuEvt");
  analysisManager->CreateNtupleDColumn("x_nuEvt");
  analysisManager->CreateNtupleDColumn("y_nuEvt");
  analysisManager->CreateNtupleDColumn("z_nuEvt");
  analysisManager->CreateNtupleIColumn("chamber",cham);
  analysisManager->CreateNtupleIColumn("iz",idz);
  analysisManager->CreateNtupleIColumn("izsub",idzsub);
  analysisManager->CreateNtupleIColumn("pdgid",pdgid);
  analysisManager->CreateNtupleIColumn("id",id);
  analysisManager->CreateNtupleIColumn("idParent",idParent);
  analysisManager->CreateNtupleDColumn("charge",charge);
  analysisManager->CreateNtupleDColumn("x",x);
  analysisManager->CreateNtupleDColumn("y",y);
  analysisManager->CreateNtupleDColumn("z",z);
  analysisManager->CreateNtupleDColumn("px",px);
  analysisManager->CreateNtupleDColumn("py",py);
  analysisManager->CreateNtupleDColumn("pz",pz);
  analysisManager->CreateNtupleDColumn("e1",e1);
  analysisManager->CreateNtupleDColumn("e2",e2);
  analysisManager->CreateNtupleDColumn("len",len);
  analysisManager->CreateNtupleDColumn("edep",edep);
  analysisManager->FinishNtuple();
//JS

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FaserRunAction::~FaserRunAction()
{
//JS
  delete G4AnalysisManager::Instance();  
//JS

  if (IsMaster()) 
  {
    RootEventIO* root = RootEventIO::GetInstance();
    root->Close();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Run* FaserRunAction::GenerateRun()
{
  return new FaserRun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FaserRunAction::BeginOfRunAction(const G4Run*)
{ 
  // inform the runManager to save random number seed
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);
//JS
  auto analysisManager = G4AnalysisManager::Instance();

  G4String fileName = "FASERnu1";
  analysisManager->OpenFile(fileName);
//JS
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FaserRunAction::EndOfRunAction(const G4Run* run)
{
//JS
  auto analysisManager = G4AnalysisManager::Instance();

  analysisManager->Write();
  analysisManager->CloseFile();
//JS  
  G4int nofEvents = run->GetNumberOfEvent();
  if (nofEvents == 0) return;

  //  note: There is no primary generator action object for "master"
  //        run manager for multi-threaded mode.
  //const FaserPrimaryGeneratorAction* generatorAction
  // = static_cast<const FaserPrimaryGeneratorAction*>
  //   (G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction());
        
  // Print
  //  
  if (IsMaster()) {
    G4cout
     << G4endl
     << "--------------------End of Global Run-----------------------";
  }
  else {
    G4cout
     << G4endl
     << "--------------------End of Local Run------------------------";
  }
  
  G4cout
     << G4endl
     << " The run consists of " << nofEvents << " events"
     << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



