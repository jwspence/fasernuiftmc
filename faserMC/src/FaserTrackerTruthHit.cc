#include "FaserTrackerTruthHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4MTRunManager.hh"
#include <iostream>

using std::cout;

G4ThreadLocal G4Allocator<FaserTrackerTruthHit>* FaserTrackerTruthHitAllocator = 0;

const FaserDetectorConstruction* FaserTrackerTruthHit::fDetectorConstruction = nullptr;
FaserTrackerTruthHit::FaserTrackerTruthHit()
    : trackID {-1}
    , plane {-1}
    , module {-1}
    , sensor {-1}
    , row {-1}
    , strip {-1}
    , energy {0.}
    , globalPos {0., 0., 0.}
  {
  }


//FaserSensorHit::FaserSensorHit()
//  : G4VHit(),
//    fPlaneID(-1),
//    fModuleID(-1),
//    fSensorID(-1),
//    fRowID(-1),
//    fStripID(-1),
//    fEdep(0.0),
//    fGlobalPos(G4ThreeVector()),
//    fLocalPos(G4ThreeVector()),
//    fTransform(G4AffineTransform()),
//    fTrackID(-1),
//    fEnergy(0),
//    fOriginTrackID(-1)
//{}

FaserTrackerTruthHit::FaserTrackerTruthHit(const FaserTrackerTruthHit& right)
  : G4VHit()
{
  plane = right.plane;
  module = right.module;
  sensor = right.sensor;
  row = right.row;
  strip = right.strip;
  energy = right.energy;
  globalPos = right.globalPos;
//  fLocalPos = right.fLocalPos;
//  fTransform = right.fTransform;
  
  trackID = right.trackID;
  energy = right.energy;
//  fOriginTrackID = right.fOriginTrackID;
}


void FaserTrackerTruthHit::print() const {

    //cout << "        SpacePoint  plane=" << plane
    //     <<                   "  globalPos=(" << globalPos.X()
    //     <<                              ", " << globalPos.Y()
    //     <<                              ", " << globalPos.Z() << ")\n";

}
